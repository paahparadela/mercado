package com.itau.mercado.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.mercado.model.Produto;
import com.itau.mercado.model.Promocao;
import com.itau.mercado.repository.ProdutoRepository;
import com.itau.mercado.repository.PromocaoRepository;

@Controller
public class PromocaoController {
	
	@Autowired
	PromocaoRepository promocaoRepository;
	
	@Autowired
	ProdutoRepository produtoRepository;
	
	@RequestMapping(path="/promocao", method=RequestMethod.POST)
	@ResponseBody
	public Promocao inserirPromocao(@RequestBody Promocao promocao) {
		Set<Produto> produtos = new HashSet<Produto>();

		for(Produto produto: promocao.getProdutos()) {
			Produto produtoDoBanco = produtoRepository.findById(produto.getLote()).get();
			produtos.add(produtoDoBanco);
		}
		
		promocao.setProdutos(produtos);
		
		return promocaoRepository.save(promocao);
	}
	
	@RequestMapping(path="/promocoes", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Promocao> getPromocoes(){
		return promocaoRepository.findAll();
	}

}
