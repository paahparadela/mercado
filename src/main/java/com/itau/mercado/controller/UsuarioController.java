package com.itau.mercado.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.mercado.model.Usuario;
import com.itau.mercado.repository.UsuarioRepository;

@Controller
public class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@RequestMapping(path="/usuario", method=RequestMethod.POST)
	@ResponseBody
	public Usuario criarUsuario(@RequestBody Usuario usuario) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		String hash = encoder.encode(usuario.getSenha());
		
		usuario.setSenha(hash);
		
		return usuarioRepository.save(usuario);
	}
	
	@RequestMapping(path="/logar", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> logar(@RequestBody Usuario usuario){
		Usuario usuarioBanco = usuarioRepository.findByEmail(usuario.getEmail());
		
		if(usuarioBanco == null) {
			return ResponseEntity.badRequest().build();
		}
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		String hash = encoder.encode(usuario.getSenha());
		
		boolean loginOk = encoder.matches(hash, usuarioBanco.getSenha());
		
		if(loginOk) {
			return ResponseEntity.ok(usuarioBanco);
		}
		
		return ResponseEntity.badRequest().build();
	}

}
