package com.itau.mercado.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.mercado.model.Produto;
import com.itau.mercado.model.Venda;
import com.itau.mercado.repository.ProdutoRepository;
import com.itau.mercado.repository.VendaRepository;

@Controller
public class VendaController {
	
	@Autowired
	VendaRepository vendaRepository;
	@Autowired
	ProdutoRepository produtoRepository;
	
	@RequestMapping(path="/venda", method=RequestMethod.POST)
	@ResponseBody
	public Venda inserirVenda(@RequestBody Venda venda) {
		
		Produto produto = new Produto();
		
		produto = venda.getProduto();
		
		Optional<Produto> produto1 = produtoRepository.findById(produto.getLote());
		
		venda.setProduto(produto1.get());
		
		venda = vendaRepository.save(venda);
		
		return venda;
	}
	
	@RequestMapping(path="/vendas", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Venda> getVendas(){
		return vendaRepository.findAll();
	}

}
