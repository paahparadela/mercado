package com.itau.mercado.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Promocao {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codPromo;
	private Date dataInicio;
	private Date dataFim;
	private Double desconto;
	
	@ManyToMany
	Set<Produto> produtos;
	
	public int getCodPromo() {
		return codPromo;
	}
	public void setCodPromo(int codPromo) {
		this.codPromo = codPromo;
	}
	public Set<Produto> getProdutos() {
		return produtos;
	}
	public void setProdutos(Set<Produto> produtos) {
		this.produtos = produtos;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public Double getDesconto() {
		return desconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	
	

}
