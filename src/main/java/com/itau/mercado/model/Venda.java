package com.itau.mercado.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Venda {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int cupom;
	private Date data;
	private Double precoEfetivo;
	
	@ManyToOne(optional=false)
	private Produto produto;
	
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Double getPrecoEfetivo() {
		return precoEfetivo;
	}
	public void setPrecoEfetivo(Double precoEfetivo) {
		this.precoEfetivo = precoEfetivo;
	}
	public int getCupom() {
		return cupom;
	}
	public void setCupom(int cupom) {
		this.cupom = cupom;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}

}
