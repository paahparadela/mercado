package com.itau.mercado.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.mercado.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
	
	public Usuario findByEmail(String email);

}
